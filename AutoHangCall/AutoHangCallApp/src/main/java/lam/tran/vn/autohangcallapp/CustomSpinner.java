/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.autohangcallapp;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * @author Lam Tran on 3/30/2014.
 */
public class CustomSpinner extends Spinner {
    private Context mContext;

    public CustomSpinner(Context context) {
        super(context);
        mContext = context;
    }

    public CustomSpinner(Context context, int mode) {
        super(context, mode);
        mContext = context;
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyle, int mode) {
        super(context, attrs, defStyle, mode);
        mContext = context;
    }

    @Override
    public boolean performClick() {
        int hour = 0;
        int min = 0;
        final SharedPreferences pref = mContext.getSharedPreferences(mContext.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        String specificTime = pref.getString(mContext.getString(R.string.shared_preference_specific_time_set), mContext.getString(R.string.row_set_timer));
        StringTokenizer tokenizer = new StringTokenizer(specificTime, ":");

        if (tokenizer.countTokens() == 2) {
            hour = Integer.parseInt(tokenizer.nextToken());
            min = Integer.parseInt(tokenizer.nextToken());
        }
        TimePickerDialog dialog = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time24H = hourOfDay + ":" + minute;
                SimpleDateFormat hh_mm = new SimpleDateFormat("HH:mm");
                SimpleDateFormat h_mm_a = new SimpleDateFormat("h:mm a");


                try {
                    Date time = hh_mm.parse(time24H);
                    setAdapter(new ArrayAdapter<>(mContext, R.layout.spinner_row_item, Arrays.asList(h_mm_a.format(time))));
                    pref.edit().putString(mContext.getString(R.string.shared_preference_specific_time_set), time24H).commit();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, hour, min, false);
        dialog.show();
        return false;
    }
}
