/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.autohangcallapp;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import lam.tran.vn.autotimers.AutoTimerReceiver;

/**
 * @author Lam Tran on 3/26/2014.
 */
public class MyReceiver extends AutoTimerReceiver {
    @Override
    public void doFunctionCall(Context context, Integer integer) {
        lam.tran.vn.autotimers.Utils.endCall(context);
        Log.d(MyReceiver.class.toString(), "Do function call");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals("lam.tran.vn.ACTION.ACTIVATE_FUNCTION")) {
                //actual start timer base on pre-set value
                Utils.doAction(context);
                Log.d(MyReceiver.class.toString(), "in activate function");

            } else if (intent.getAction().equals("lam.tran.vn.ACTION.DEACTIVATE_FUNCTION")) {
                // switch notification to have activate button
                Utils.showNotification(context, false);
                // deactivated and start timer
                Utils.cancelTimer(context);
                Log.d(MyReceiver.class.toString(), "in deactivate function");

            } else if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {

                // start AutoHangCall notification
                Utils.startAutoHangOnType(context);
                Log.d(MyReceiver.class.toString(), "in new outgoing call");

            } else if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {

                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                if (TelephonyManager.EXTRA_STATE_RINGING.equals(state)) {
                    // String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    Utils.startAutoHangOnType(context);
                    Log.d(MyReceiver.class.toString(), "in ringing state ");

                } else if (TelephonyManager.EXTRA_STATE_IDLE.equals(state)) {
                    Utils.cancelNotification(context, context.getResources().getInteger(R.integer.app_notification_id));
                    Log.d(MyReceiver.class.toString(), "in idle state ");
                }
            }
        }
    }
}

