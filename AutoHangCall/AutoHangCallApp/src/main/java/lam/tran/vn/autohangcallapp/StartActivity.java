/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.autohangcallapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * @author Lam Tran
 */
public class StartActivity extends Activity {
    Spinner mEnableSpinner;
    CustomSpinner mTimeSetSpinner;
    LinearLayout mTimeSetLayout;
    RadioButton mRadioButton1;
    RadioButton mRadioButton2;
    EditText mEditText;
    RadioGroup mRadioGroup;
    Button startBtn;
    Button endBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        mEnableSpinner = (Spinner) findViewById(R.id.enable_spinner);
        mTimeSetSpinner = (CustomSpinner) findViewById(R.id.time_set_spinner);
        mTimeSetLayout = (LinearLayout) findViewById(R.id.time_set_layout);
        mRadioButton1 = (RadioButton) findViewById(R.id.radio_span);
        mRadioButton2 = (RadioButton) findViewById(R.id.radio_specific_time);
        mEditText = (EditText) findViewById(R.id.time_set_edit_text);
        mRadioGroup = (RadioGroup) findViewById(R.id.radio_group);

        final SharedPreferences pref = getSharedPreferences(getString(R.string.shared_preference_name), MODE_PRIVATE);
        int enableId = pref.getInt(getString(R.string.shared_preference_enable_type), 0);

        // build spinner for enable type spinner
        mEnableSpinner.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_row_item,
                Arrays.asList(getString(R.string.row_spinner_disable),
                        getString(R.string.row_spinner_enable_but_not_activated),
                        getString(R.string.row_spinner_enable_and_activate))
        ));
        mEnableSpinner.setSelection(enableId);

        mEnableSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pref.edit().putInt(getString(R.string.shared_preference_enable_type), (int) mEnableSpinner.getSelectedItemId()).commit();

                if (mEnableSpinner.getSelectedItemId() == 0) {
                    mEditText.setEnabled(false);
                    mTimeSetSpinner.setEnabled(false);
                    mRadioButton1.setEnabled(false);
                    mRadioButton2.setEnabled(false);
                } else {
                    // enable
                    if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_span) {
                        // time span
                        mEditText.setEnabled(true);
                        mTimeSetSpinner.setEnabled(false);
                    } else {
                        // specific time
                        mEditText.setEnabled(false);
                        mTimeSetSpinner.setEnabled(true);
                    }
                    mRadioButton1.setEnabled(true);
                    mRadioButton2.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Set up time span
        String timeSpan = pref.getString(getString(R.string.shared_preference_time_span_set), null);
        if (timeSpan != null) mEditText.setText(timeSpan);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pref.edit().putString(getString(R.string.shared_preference_time_span_set), s.toString()).commit();

            }
        });

        // set up specific time spinner
        SimpleDateFormat hh_mm = new SimpleDateFormat("HH:mm");
        SimpleDateFormat h_mm_a = new SimpleDateFormat("h:mm a");
        String specificTime = pref.getString(getString(R.string.shared_preference_specific_time_set), getString(R.string.row_set_timer));

        String timeShown = null;
        try {
            Date time = hh_mm.parse(specificTime);
            timeShown = h_mm_a.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mTimeSetSpinner.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_row_item, Arrays.asList(timeShown != null ? timeShown : specificTime)));

        // set up radio buttons
        int timeSetType = pref.getInt(getString(R.string.shared_preference_time_set_type), R.id.radio_span);
        mRadioGroup.check(timeSetType);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                pref.edit().putInt(getString(R.string.shared_preference_time_set_type), checkedId).commit();
                if (checkedId == R.id.radio_span) {
                    // time span
                    mEditText.setEnabled(true);
                    mTimeSetSpinner.setEnabled(false);
                } else {
                    // specific time
                    mEditText.setEnabled(false);
                    mTimeSetSpinner.setEnabled(true);
                }
            }
        });

        startBtn = (Button) findViewById(R.id.startBtn);
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.startAutoHangOnType(StartActivity.this);
            }
        });

        endBtn = (Button) findViewById(R.id.endBtn);
        endBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.cancelTimer(StartActivity.this);
                Utils.cancelNotification(StartActivity.this, getResources().getInteger(lam.tran.vn.autotimers.R.integer.app_notification_id));
            }
        });
    }
}
