/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.autohangcallapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;

import lam.tran.vn.autotimers.TimerFunction;

/**
 * @author Lam Tran on 4/1/2014.
 */
public class Utils {
    public static void startAutoHangOnType(Context context) {
        final SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        int enableId = pref.getInt(context.getString(R.string.shared_preference_enable_type), 0);
        switch (enableId) {
            case 0:
                // disable state
                return;

            case 1:
                // enable but not activate state
                showNotification(context, false);
                break;

            case 2:
                context.sendBroadcast(new Intent("lam.tran.vn.ACTION.ACTIVATE_FUNCTION"));
                break;

            default:
                break;
        }
    }

    public static void doAction(Context context) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm a");
        final SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        int timeSetType = pref.getInt(context.getString(R.string.shared_preference_time_set_type), R.id.radio_span);
        if (timeSetType == R.id.radio_span) {
            String timeSpan = pref.getString(context.getString(R.string.shared_preference_time_span_set), null);
            int time = 0;
            try {
                time = Integer.parseInt(timeSpan);
            } catch (Exception e) {

            }

            if (time > 0) {
                TimerFunction timerFunction = new TimerFunction(context.getResources().getInteger(R.integer.timer_function_id), context, time);
                timerFunction.startTimer(0);
                showNotification(context, true, dateFormat.format(timerFunction.getTimeCall()));

            }
        } else if (timeSetType == R.id.radio_specific_time) {
            String specificTime = pref.getString(context.getString(R.string.shared_preference_specific_time_set), context.getString(R.string.row_set_timer));
            StringTokenizer tokenizer = new StringTokenizer(specificTime, ":");
            if (tokenizer.countTokens() != 2) {
                Toast.makeText(context, "Wrong time set, please reset specific time", Toast.LENGTH_SHORT).show();
            } else {
                Calendar calendar = Calendar.getInstance();
                int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                int hour = Integer.parseInt(tokenizer.nextToken());
                int min = Integer.parseInt(tokenizer.nextToken());

                if (currentHour > hour) {
                    // in the next date
                    calendar.add(Calendar.DATE, 1);
                }

                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, min);
                calendar.set(Calendar.SECOND, 0);

                TimerFunction timerFunction = new TimerFunction(context.getResources().getInteger(R.integer.timer_function_id), context, calendar);
                timerFunction.startTimer(0);
                showNotification(context, true, dateFormat.format(calendar.getTime()));
            }
        }
    }

    public static void showNotification(Context context, boolean isEnable, String... endCallTime) {
        String functionAction = isEnable ? "lam.tran.vn.ACTION.DEACTIVATE_FUNCTION" : "lam.tran.vn.ACTION.ACTIVATE_FUNCTION";
        PendingIntent actionIntent = PendingIntent.getBroadcast(context, 0, new Intent(functionAction), PendingIntent.FLAG_UPDATE_CURRENT);
        String messageContent;

        if (isEnable) {
            messageContent = context.getString(R.string.noti_content_enable, endCallTime.length > 0 ? endCallTime[0] : "");
        } else {
            messageContent = context.getString(R.string.noti_content_disable);
        }

        Notification notification = new Notification.Builder(context)
                .setContentTitle(context.getString(lam.tran.vn.autotimers.R.string.app_name))
                .setContentText(messageContent)
                .setSmallIcon(lam.tran.vn.autotimers.R.drawable.ic_launcher)
                .setContentIntent(actionIntent)
                .setAutoCancel(false)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notificationManager.notify(context.getResources().getInteger(lam.tran.vn.autotimers.R.integer.app_notification_id), notification);
    }

    public static void cancelNotification(Context context, int notificationId) {
        if (context.getApplicationContext() == null) {
            return;
        }

        NotificationManager nMgr = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(notificationId);
    }

    public static void cancelTimer(Context context) {
        TimerFunction timerFunction = new TimerFunction(context.getResources().getInteger(R.integer.timer_function_id), context, 0);
        timerFunction.cancelTimer();
    }
}
