/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.autotimers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author Lam Tran on 3/25/2014.
 */
public class AutoTimerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null || !intent.getAction().equals("lam.tran.vn.ACTION.CALL_FUNCTION")) {
            return;
        }

        int id = intent.getIntExtra("intent_id", -1);
        int selection = intent.getIntExtra("selection", -1);
        if (id >= 0 && selection >= 0) {
            doFunctionCall(context, selection);
        }
    }

    public void doFunctionCall(Context context, Integer index) {
    }
}
