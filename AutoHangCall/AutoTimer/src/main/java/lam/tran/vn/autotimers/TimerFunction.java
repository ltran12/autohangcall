/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.autotimers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.Calendar;

/**
 * @author Lam Tran on 3/25/2014.
 */
public class TimerFunction {
    private int mTimeInterval;
    private Calendar mCalendar;
    private Context mContext;
    private int mCurrentId;

    /**
     * Set time interval in mins
     *
     * @param min time to call function in mins
     */
    public TimerFunction(int id, Context context, int min) {
        mTimeInterval = min;
        initTimer(context, id);
    }

    public TimerFunction(int id, Context context, int hour, int min) {
        mTimeInterval = (hour * 60) + min;
        initTimer(context, id);
    }

    public TimerFunction(int id, Context context, int day, int hour, int min) {
        mTimeInterval = (day * 24 * 60) + (hour * 60) + min;
        initTimer(context, id);
    }

    /**
     * Set time interval in mins
     *
     * @param calendar calendar in utc timezone
     */
    public TimerFunction(int id, Context context, Calendar calendar) {
        Calendar curCal = Calendar.getInstance();

        long diff = calendar.getTimeInMillis() - curCal.getTimeInMillis();
        mTimeInterval = (int) (diff / (1000 * 60));
        initTimer(context, id);
    }

    private void initTimer(Context context, int id) {
        mCurrentId = id;
        mContext = context;
    }

    private int generateId() {
        SharedPreferences pref = mContext.getSharedPreferences("id_stored", Context.MODE_PRIVATE);
        int id = pref.getInt("intent_id", 0);
        pref.edit().putInt("intent_id", ++id).commit();
        return id;
    }

    /**
     * start alarm manager to do function call
     */
    public int startTimer(int selection) {
        if (mCurrentId < 0) {
            mCurrentId = generateId();
        }

        mCalendar = Calendar.getInstance();
        mCalendar.add(Calendar.MINUTE, mTimeInterval);

        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent("lam.tran.vn.ACTION.CALL_FUNCTION");
        intent.putExtra("intent_id", mCurrentId);
        intent.putExtra("selection", selection);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, mCurrentId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + mTimeInterval * 60 * 1000, pendingIntent);

        return mCurrentId;
    }

    public void cancelTimer() {
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent("lam.tran.vn.ACTION.CALL_FUNCTION");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, mCurrentId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }


    public int getTimeInterval() {
        return mTimeInterval;
    }

    public Calendar getTimeCall() {
        return mCalendar;
    }

    public int getId() {
        return mCurrentId;
    }
}
