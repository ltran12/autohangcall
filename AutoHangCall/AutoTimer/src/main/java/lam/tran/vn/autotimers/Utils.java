/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.autotimers;

import android.content.Context;
import android.os.RemoteException;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Lam Tran on 3/24/2014.
 */
public class Utils {
    /**
     * FUnction to end call
     *
     * @param context context - context of the app
     */
    public static void endCall(final Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        Class clazz;
        try {
            clazz = Class.forName(telephonyManager.getClass().getName());
            Method method;
            method = clazz.getDeclaredMethod("getITelephony");
            method.setAccessible(true);
            ITelephony telephonyService;

            telephonyService = (ITelephony) method.invoke(telephonyManager);
            telephonyService.endCall();


        } catch (RemoteException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

//    public void showNotification(Context context, boolean isEnable, PendingIntent configIntent, PendingIntent actionIntent) {
//        Notification notification = new Notification.Builder(context)
//                .setContentTitle(context.getString(R.string.app_name))
//                .setContentText("Here's an awesome update for you!")
//                .setSmallIcon(R.drawable.ic_launcher)
//                .setContentIntent(configIntent)
//                .setAutoCancel(true)
//                .addAction(0, isEnable ? context.getString(R.string.noti_disable) : context.getString(R.string.noti_enable), actionIntent)
//                .build();
//
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notification.flags |= Notification.FLAG_NO_CLEAR;
//        notificationManager.notify(context.getResources().getInteger(R.integer.app_notification_id), notification);
//    }
//
//    public void cancelNotification(Context context, int notificationId) {
//        if (context == null || context.getApplicationContext() == null) {
//            return;
//        }
//
//        NotificationManager nMgr = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//        nMgr.cancel(notificationId);
//    }
}
